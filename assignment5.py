import os
import pickle


class ConfigDict(dict):
    config_directory = './test/'
    _filepath = ''

    def __init__(self, config_name):
        super().__init__()
        self._filepath = os.path.join(self.config_directory, config_name + '.pickle')
        if os.path.isfile(self._filepath):
            with open(self._filepath, 'rb') as file:
                pickled_dict = pickle.load(file)
                dict.update(pickled_dict)
        else:
            try:
                with open(self._filepath, 'wb') as file:
                    pickle.dump({}, file)
            except IOError:
                raise IOError('path does not exist')

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        if self._filepath != '':
            with open(self._filepath, 'wb') as file:
                pickle.dump(self, file)

    def __getitem__(self, key):
        try:
            return dict.__getitem__(self, key)
        except KeyError:
            raise ConfigKeyError(self, key)


class ConfigKeyError(Exception):
    def __init__(self, cd, key):
        self.cd = cd
        self.key = key

    def __str__(self):
        return "key \"{0}\" not found. Available keys: ({1})".format(self.key, ','.join(list(self.cd.keys())))
