class MaxSizeList(object):
    def __init__(self, size):
        self.arr = []
        self.size = size

    def push(self, val):
        if len(self.arr) < self.size:
            self.arr.append(val)
        else:
            self.arr.pop(0)
            self.arr.append(val)

    def get_list(self):
        return self.arr


a = MaxSizeList(3)
b = MaxSizeList(1)

a.push(1)
a.push(2)
a.push(3)
a.push(4)

b.push(1)
b.push(2)

print(a.get_list())
print(b.get_list())
