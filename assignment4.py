import os


class ConfigDict(dict):
    def __init__(self, file_name):
        super().__init__()
        self._file_name = file_name
        if os.path.isfile(file_name):
            with open(file_name, 'r') as file:
                for line in file:
                    line = line.strip()
                    key, value = line.split('=', 1)
                    dict.__setitem__(self, key, value)
        else:
            try:
                file = open(file_name, 'w')
                file.close()
            except IOError:
                raise IOError('path does not exist')

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        with open(self._file_name, 'w') as file:
            for key, val in self.items():
                line = "{0}={1}".format(key, val)
                file.write(line + '\n')

    def __getitem__(self, key):
        try:
            return dict.__getitem__(self, key)
        except KeyError:
            raise ConfigKeyError(self, key)


class ConfigKeyError(Exception):
    def __init__(self, cd, key):
        self.cd = cd
        self.key = key

    def __str__(self):
        return "key \"{0}\" not found. Available keys: ({1})".format(self.key, ','.join(list(self.cd.keys())))
