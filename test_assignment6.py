from assignment4 import ConfigDict, ConfigKeyError
import pytest
import shutil
import os


class TestConfigDict:
    dict_template = 'test/test_dict_template.txt'
    dict_testor = 'test/test.txt'
    new_file_name = 'new_file.txt'
    bad_file_path = 'bad/no.txt'

    def setup(self):
        shutil.copy(self.dict_template, self.dict_testor)

    def teardown(self):
        os.remove(self.dict_testor)

    def test_obj(self):
        cd = ConfigDict(self.dict_testor)
        assert isinstance(cd, ConfigDict)
        assert isinstance(cd, dict)

    def test_filename(self):
        cd = ConfigDict(self.dict_testor)
        assert cd._file_name == self.dict_testor

    def test_pare_existence(self):
        cd = ConfigDict(self.dict_testor)
        assert os.path.isfile(self.dict_testor) is True

    def test_new_existence(self, tmpdir):
        filepath = tmpdir.mkdir('test').join(self.new_file_name)
        cd = ConfigDict(filepath)
        assert os.path.isfile(filepath) is True

    def test_bad_filepath(self):
        with pytest.raises(IOError):
            cd = ConfigDict(self.bad_file_path)

    def test_correct_values(self):
        cd = ConfigDict(self.dict_testor)
        assert cd['a'] == '1'
        assert cd['b'] == '2'
        assert cd['c'] == 'hello'
        assert cd['4'] == 'd'
        assert cd['equal'] == 'this=that'

    def test_key_read(self):
        cd = ConfigDict(self.dict_testor)
        bad_key = 'bad'
        with pytest.raises(ConfigKeyError):
            cd = ConfigDict(self.dict_testor)
            print(cd[bad_key])

    def test_adding_values(self):
        cd = ConfigDict(self.dict_testor)
        cd['add'] = 'test'
        new_cd = ConfigDict(self.dict_testor)
        assert new_cd['add'] == 'test'
