import datetime


class WriteFile(object):
    def __init__(self, filename, writer):
        self.filename = filename
        self.writer = writer()

    def write(self, info):
        with open(self.filename, 'a') as file:
            self.writer.write(file, info)


class DelimFile(object):
    def __init__(self, delimiter=','):
        self.delimiter = delimiter

    def write(self, file, info: list):
        quoted_info = self.quote(info)
        new_info = self.delimiter.join(quoted_info)
        file.write(new_info + '\n')

    def quote(self, info: list):
        info = map(str, info)
        quoted_info = []
        for cell in info:
            if self.delimiter in cell:
                quoted_info.append('\"{0}\"'.format(cell))
            else:
                quoted_info.append(cell)
        return quoted_info


class LogFile(object):
    def write(self, file, info):
        dt_str = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
        file.write('{0}  {1}\n'.format(dt_str, info))
