import os


class ConfigDict(dict):
    def __init__(self, file_name):
        super().__init__()
        self.file_name = file_name
        if os.path.isfile(file_name):
            with open(file_name, 'r') as file:
                config = file.readlines()
                for line in config:
                    key, value = line.split('=', 1)
                    dict.__setitem__(self, key, value)

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        with open(self.file_name, 'w') as file:
            for key in self.keys():
                line = "{0}={1}".format(key, dict.__getitem__(self, key))
                file.write(line + '\n')
