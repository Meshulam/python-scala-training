import abc
import datetime
import csv


class WriteFile(object, metaclass=abc.ABCMeta):

    def __init__(self, file_name):
        self.file_name = file_name

    @abc.abstractmethod
    def write(self, info):
        return


class LogFile(WriteFile):
    def write(self, info):
        dt_str = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
        with open(self.file_name, 'a') as file:
            file.write(dt_str + ' ' + info + '\n')


class DelimFile(WriteFile):
    def __init__(self, file_name, delimiter):
        super(DelimFile, self).__init__(file_name)
        self.delimiter = delimiter

    def write(self, info):
        with open(self.file_name, 'a') as file:
            csv.writer(file, delimiter=self.delimiter).writerow(info)
